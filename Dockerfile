# Use an OpenJDK runtime as a parent image
FROM openjdk:11-jre-slim
# Set the working directory to /app
WORKDIR /app
# Make port 8080 available to the world outside this container
EXPOSE 8080
# Run the Spring Boot application
CMD ["java", "-jar", "app.jar"]
